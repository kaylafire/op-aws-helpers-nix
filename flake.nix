{
	description = "1Password AWS Helpers";

	inputs.devshell.url = "github:numtide/devshell";
	inputs.devshell.inputs.nixpkgs.follows = "/nixpkgs";

	inputs.flake-parts.url = "github:hercules-ci/flake-parts";
	inputs.flake-parts.inputs.nixpkgs-lib.follows = "/nixpkgs";

	outputs = inputs@{ self, flake-parts, devshell, nixpkgs }:
	flake-parts.lib.mkFlake { inherit inputs; } {
		imports = [
			devshell.flakeModule
		];

		systems = [
			"aarch64-darwin"
			"aarch64-linux"
			"i686-linux"
			"x86_64-darwin"
			"x86_64-linux"
		];

		perSystem = { pkgs, self', ... }: {
			packages.default = self'.packages.op-aws-rotate;
			packages.op-aws-rotate = pkgs.callPackage ./op-aws-rotate.nix {};

			checks = self'.packages;

			devshells.default = {
				packages = [
					pkgs.python3
					pkgs.awscli2
					pkgs._1password
				];
			};
		};
	};
}
